<?php
  $template = 'single';
  $template_title = 'Reception';
  $template_theme = 'wedding';
?>
<?php include("inc/header.php"); ?>
<h2>The Reception</h2>
<p><strong>Templeton Landing</strong></p>
<p>2 Templeton Terrace</p>
<p>Buffalo, NY</p>
<img src="img/templetonlanding-image.jpg" class="float-none">
<h3>Parking</h3>
<p>There will be valet parking for all guests at the reception. Pull up to the archway in front of the restaurant and the valet will take care of the rest.</p>
<?php include("inc/footer.php"); ?>